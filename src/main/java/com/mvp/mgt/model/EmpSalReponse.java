package com.mvp.mgt.model;


import org.springframework.http.HttpStatus;

import com.mvp.mgt.entity.EmpSalary;

import lombok.Data;

@Data
public class EmpSalReponse {

	private HttpStatus status;
	private String message;
	private EmpSalary empSal;
}
