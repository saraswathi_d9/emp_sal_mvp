package com.mvp.mgt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EntityScan("com.mvp.mgt.entity")
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class MVPApplication {

	public static void main(String[] args) {
		SpringApplication.run(MVPApplication.class, args);
	}

}
