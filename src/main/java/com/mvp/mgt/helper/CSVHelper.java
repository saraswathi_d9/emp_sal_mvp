package com.mvp.mgt.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import com.mvp.mgt.entity.EmpSalary;

public class CSVHelper {
  public static String TYPE = "application/csv";
  static String[] HEADERs = { "id", "name","login", "salary", "startDate" };

  public static boolean hasCSVFormat(MultipartFile file) {

    if (!TYPE.equals(file.getContentType())) {
      return false;
    }

    return true;
  }

  public static List<EmpSalary> csvToTutorials(InputStream is) throws ParseException {
    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        CSVParser csvParser = new CSVParser(fileReader,
            CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

      List<EmpSalary> tutorials = new ArrayList<EmpSalary>();

      Iterable<CSVRecord> csvRecords = csvParser.getRecords();

      for (CSVRecord csvRecord : csvRecords) {
        EmpSalary tutorial = new EmpSalary();
        tutorial.setId(csvRecord.get("id"));
        tutorial.setName(csvRecord.get("name"));
        tutorial.setLogin(csvRecord.get("login"));
        tutorial.setSal(Float.parseFloat(csvRecord.get("salary")));
        tutorial.setDate(csvRecord.get("startDate"));
              
        tutorials.add(tutorial);
        System.out.println("tutorial:"+tutorial);
      };

      return tutorials;
    } catch (IOException e) {
      throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
    }
  }

}