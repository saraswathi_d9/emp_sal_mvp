package com.mvp.mgt.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.mvp.mgt.entity.EmpSalary;
import com.mvp.mgt.helper.CSVHelper;
import com.mvp.mgt.model.EmpSalReponse;
import com.mvp.mgt.service.EmpSalaryService;

@RestController
@RequestMapping("/")
public class EmpSalController {

	@Autowired
	EmpSalaryService empSalaryservice;
	@GetMapping(value="/test")
	public String test() {
		return "hello";
	}
	@PostMapping("/api/upload")
	@ResponseBody
    public ResponseEntity<EmpSalReponse> uploadFile(@Valid @RequestParam("file") MultipartFile uploadfile) throws Exception {
			try {
				CSVHelper.hasCSVFormat(uploadfile);
				List<EmpSalary> empList=CSVHelper.csvToTutorials(uploadfile.getInputStream());
				System.out.println("emplist"+empList);
				empList.forEach(e-> empSalaryservice.processEmpSal(e));
			} 
			catch (ParseException | IOException e) {
				return new ResponseEntity<EmpSalReponse>(HttpStatus.NOT_FOUND);
			}catch(Exception e) {

				return new ResponseEntity<EmpSalReponse>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			 EmpSalReponse empSalResponse = new EmpSalReponse();
			
			empSalResponse.setMessage("Successfully uploaded");
			 empSalResponse.setStatus(HttpStatus.OK);
			return new ResponseEntity<EmpSalReponse>(HttpStatus.OK);
       
    }
	@PutMapping("users/{id}")
	public ResponseEntity<EmpSalReponse> updateUsers(@RequestBody EmpSalary emp,@PathVariable("id") String id) {
		EmpSalary empSalary=empSalaryservice.updateEmpSalary(emp,id);	

		 EmpSalReponse empSalResponse = new EmpSalReponse();
		 empSalResponse.setEmpSal(empSalary);
		 empSalResponse.setMessage("Successfully updated");
		 empSalResponse.setStatus(HttpStatus.OK);
		if(empSalary.getId().equals(id)) {
			return new ResponseEntity<EmpSalReponse>(empSalResponse, HttpStatus.OK);
		}else {
			empSalResponse.setMessage(" Bad input - no such employee, login not unique etc.");
			 empSalResponse.setStatus(HttpStatus.BAD_REQUEST);
				return new ResponseEntity<EmpSalReponse>(empSalResponse, HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping("users/{id}")
	public ResponseEntity<EmpSalReponse> getUsersById(@PathVariable("id") String id) {
		Optional<EmpSalary> empSalary = empSalaryservice.getEmpSalaryId(id);
		System.out.println("Hello"+empSalary);

		 EmpSalReponse empSalResponse = new EmpSalReponse();
		 
		EmpSalary empSal = null;
		if(empSalary.isPresent()) {
			System.out.println("Hello3");
			 empSal = empSalary.get();
				empSalResponse.setEmpSal(empSal);
				empSalResponse.setMessage("Success but no data updated");
				empSalResponse.setStatus(HttpStatus.OK);
		       
		return new ResponseEntity<EmpSalReponse>(empSalResponse, HttpStatus.OK);
		}else {
			empSalResponse.setMessage("Bad input - no such employee");
			empSalResponse.setStatus(HttpStatus.BAD_REQUEST);
	        
			return new ResponseEntity<EmpSalReponse>(empSalResponse, HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/users")
	public ResponseEntity<EmpSalReponse> addUsers(@RequestBody EmpSalary empSalary) {
		EmpSalary empSal= empSalaryservice.addEmpSalary(empSalary);
		EmpSalReponse empSalResponse = new EmpSalReponse();
		empSalResponse.setEmpSal(empSal);
		empSalResponse.setMessage("New employee record created.");
		empSalResponse.setStatus(HttpStatus.CREATED);
                return new ResponseEntity<EmpSalReponse>(empSalResponse, HttpStatus.CREATED);
	}
	
	@DeleteMapping("users/{id}")
	public ResponseEntity<EmpSalReponse> deleteUsers(@PathVariable("id") String id) {
		EmpSalReponse empSalResponse = new EmpSalReponse();
		
		try {
		empSalaryservice.deleteEmpById(id);
		empSalResponse.setMessage("deleted successfully");
		return new ResponseEntity<EmpSalReponse>(empSalResponse,HttpStatus.OK);
		}catch(Exception e) {
			empSalResponse.setMessage("No such Employee");
	          
		return new ResponseEntity<EmpSalReponse>(empSalResponse,HttpStatus.BAD_REQUEST);
		}
	
	}	 
	@GetMapping("users")
	public ResponseEntity<List<EmpSalary>> getUsers( @RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy,@RequestParam(defaultValue = "0") Float minSalary,@RequestParam(defaultValue = "4000.00") Float maxSalary) {
			
				List<EmpSalary> list = empSalaryservice.getAllEmployees(pageNo, pageSize, sortBy,minSalary,maxSalary);
		 
	        return new ResponseEntity<List<EmpSalary>>(list, HttpStatus.OK); 
			
	}
}
