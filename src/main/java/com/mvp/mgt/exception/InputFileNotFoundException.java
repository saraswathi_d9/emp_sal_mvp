package com.mvp.mgt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
@ResponseStatus(HttpStatus.NOT_FOUND)

public class InputFileNotFoundException extends RuntimeException  {
	    /**
	 * 
	 */
	private static final long serialVersionUID = -9204011362587920406L;

		public InputFileNotFoundException(String exception) {
	        super(exception);
	}
}
