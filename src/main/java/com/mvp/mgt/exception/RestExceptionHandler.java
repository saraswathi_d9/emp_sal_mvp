package com.mvp.mgt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
 
    @ExceptionHandler({ InputFileNotFoundException.class })
    protected ResponseEntity<Object> fileNotFound(String s) {
        return new ResponseEntity<Object>(s,HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
