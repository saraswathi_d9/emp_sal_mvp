package com.mvp.mgt.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mvp.mgt.entity.EmpSalary;
@Service
public interface EmpSalaryService {
	public Optional<EmpSalary> validateLogin(String login);
	public void processEmpSal(EmpSalary emp);
	public void deleteEmpById(String id);
	public EmpSalary addEmpSalary(EmpSalary empSalary);
	public List<EmpSalary> getAllEmpSalary();
	public EmpSalary updateEmpSalary(EmpSalary empSal,String id);
	public Optional<EmpSalary> getEmpSalaryId(String id);
	public List<EmpSalary> getAllEmployees(Integer pageNo, Integer pageSize, String sortBy,Float minSal,Float maxSal);

}
