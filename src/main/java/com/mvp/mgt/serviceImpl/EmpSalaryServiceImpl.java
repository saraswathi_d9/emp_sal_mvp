package com.mvp.mgt.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;

import com.mvp.mgt.entity.EmpSalary;
import com.mvp.mgt.repo.EmpSalaryRepo;
import com.mvp.mgt.service.EmpSalaryService;
@Component
public class EmpSalaryServiceImpl implements EmpSalaryService{
	
	@Autowired
	EmpSalaryRepo empSalaryRepo;
	@Override
	public Optional<EmpSalary> validateLogin(String login) {
		return empSalaryRepo.findById(login);
	}

	@Override
	@Transactional
	public void processEmpSal(EmpSalary emp) {
			empSalaryRepo.save(emp);
	}

	@Override
	public void deleteEmpById(String id) {
		empSalaryRepo.deleteById(id);
	}

	@Override
	public EmpSalary addEmpSalary(EmpSalary empSalary) {
		return empSalaryRepo.save(empSalary);
	}

	@Override
	public List<EmpSalary> getAllEmpSalary() {
		List<EmpSalary> empList = new ArrayList<EmpSalary>();
		Iterable<EmpSalary> empIt=empSalaryRepo.findAll();
		empIt.forEach(empList::add);
		return empList;
	}


	@Override
	public EmpSalary updateEmpSalary(EmpSalary empSal, String id) {
		Optional<EmpSalary> empSalary1=empSalaryRepo.findById(empSal.getId());
			     if(empSalary1.isPresent()) {
			    	 EmpSalary empSalary = new EmpSalary();			 		
			    	 empSalary.setName(empSal.getName());
			    	 empSalary.setLogin(empSal.getLogin());
			    	 empSalary.setDate(empSal.getDate());
			    	 empSalary.setSal(empSal.getSal());
			    	 empSalary.setId(empSal.getId());			    	 
		    	 return empSalaryRepo.save(empSalary);
			     }else {
			    	 return empSalaryRepo.save(empSal);
			      }	
	}

	public Optional<EmpSalary> getEmpSalaryId(String id) {
		System.out.println("service");
		return empSalaryRepo.findById(id);
	}

	@Override
	public List<EmpSalary> getAllEmployees(Integer pageNo, Integer pageSize, String sortBy,Float minSal,Float maxSal) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		 
        Page<EmpSalary> pagedResult = empSalaryRepo.findBySalBetween(minSal,maxSal,paging);
         
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<EmpSalary>();
        }
    }
}
