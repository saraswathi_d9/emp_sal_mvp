package com.mvp.mgt.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mvp.mgt.entity.EmpSalary;

@Repository
public interface EmpSalaryRepo extends PagingAndSortingRepository<EmpSalary, String>{
	
	Page<EmpSalary> findBySalBetween(Float minSal, Float maxSal, Pageable paging);

}
