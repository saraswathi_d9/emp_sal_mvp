package com.mvp.mgt.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
@Data
@Entity
public class EmpSalary {
@Id
private String id;
@NotBlank(message = "Please provide name")
private String name;
@NotBlank(message = "Please provide login")
private String login;
@NotNull(message = "Please provide a price")
@DecimalMin("0.0")
private Float sal;
@NotBlank(message = "Please provide date")
private String date;
}
