package com.mvp.mgt;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mvp.mgt.entity.EmpSalary;
import com.mvp.mgt.service.EmpSalaryService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class MVPApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	EmpSalaryService empSalaryService;
	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		mockMvc.perform(get("/test")
			    .contentType("application/json"))
			    .andExpect(status().isOk()).andReturn().equals("hello");
		}
	@Test
	public void testGetUsersById() throws Exception {
		String userId="e0001";
		EmpSalary empSal= new EmpSalary();
		empSal.setId("e0001");
		empSal.setName("test");
		when(empSalaryService.getEmpSalaryId(userId)).thenReturn(Optional.of(empSal));
		mockMvc.perform(get("/users/{id}",userId)
			    .contentType("application/json"))
			    .andExpect(status().isOk()).andExpect(jsonPath("$.message",is("Success but no data updated")));
		}
	
	@Test
	public void testAddUsers() throws Exception {
		EmpSalary empSal= new EmpSalary();
		empSal.setId("e0001");
		empSal.setName("test");
		String requestBody = new ObjectMapper().valueToTree(empSal).toString();
		when(empSalaryService.addEmpSalary(empSal)).thenReturn(empSal);
		mockMvc.perform(post("/users",empSal)
				.content(requestBody)
			    .contentType("application/json"))
			    .andExpect(status().is2xxSuccessful()).andExpect(jsonPath("$.message",is("New employee record created.")));
		}
	@Test
	public void testUpdateUsers() throws Exception {
		String userId="e0001";
		EmpSalary empSal= new EmpSalary();
		empSal.setId("e0001");
		empSal.setName("test");
		String requestBody = new ObjectMapper().valueToTree(empSal).toString();
		when(empSalaryService.updateEmpSalary(empSal,userId)).thenReturn(empSal);
		mockMvc.perform(put("/users/{id}",userId)
				.content(requestBody)
			    .contentType("application/json"))
			    .andExpect(status().is2xxSuccessful()).andExpect(jsonPath("$.message",is("Successfully updated")));
		}
	
	@Test
	public void testDeleteUsers() throws Exception {
		String userId="e0001";
		EmpSalary empSal= new EmpSalary();
		empSal.setId("e0001");
		empSal.setName("test");
		doNothing().when(empSalaryService).deleteEmpById(userId);
		mockMvc.perform(delete("/users/{id}",userId)
			    .contentType("application/json"))
			    .andExpect(status().isOk()).andExpect(jsonPath("$.message",is("deleted successfully")));
		}
	@Test
	public void testGetUsers() throws Exception {
		Integer pageNo=1;
		Integer pageSize =1;
		String sortBy="name";
		Float minSalary=(float) 0;
		Float maxSalary=(float) 4000.00;
		EmpSalary empSal= new EmpSalary();
		empSal.setId("e0001");
		empSal.setName("test");
		List<EmpSalary> empList= new ArrayList<EmpSalary>();
		empList.add(empSal);
		EmpSalary empSal1= new EmpSalary();
		empSal.setId("e0002");
		empSal.setName("sss");
		empList.add(empSal1);
		when(empSalaryService.getAllEmployees(pageNo, pageSize, sortBy,minSalary,maxSalary)).thenReturn(empList);
		mockMvc.perform(get("/users")
			    .contentType("application/json"))
			    .andExpect(status().isOk());
		}
}
