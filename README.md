# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Prerequisities ###

* Java 8
* Spring Boot
* Maven 3

### How do I get set up? ###

* Summary of set up
```
clone code from repo: git clone https://saraswathi_d9@bitbucket.org/saraswathi_d9/emp_sal_mvp.git
```
* Maven project Configuration
```
mvn clean 
mvn clean install
```
* Dependencies
* Database configuration
```
can check the db below console once started the app
http://localhost:8080/h2-console
```
* How to run tests
```
mvn clean test
```
* Deployment instructions

start the spring boot app with below command
```
mvn spring-boot:run
```

* Run the below API's from postman
```
USER Story-1
save sample data in .csv file and form data as file , attach the .csv file
GET: http://localhost:8080/api/upload
USER Stroy-2 
GET: http://localhost:8080/users?pageNo=0&pageSize=10&sortBy=id&minSalary=200&maxSalary=5000
USER Stroy-3
GET: http://localhost:8080/users/e0001
POST: http://localhost:8080/users
PUT: http://localhost:8080/users/e0002
DELETE: http://localhost:8080/users/e0001
```